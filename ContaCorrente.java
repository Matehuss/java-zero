package Conta;

public class ContaCorrente extends Conta{
	private double chequeEspecial;

	@Override
	public String toString() {
		return "ContaCorrente [chequeEspecial=" + chequeEspecial + "]";
	}

	public ContaCorrente(String nome, int numero, int agencia, String banco, double saldo, double chequeEspecial) {
		super(nome, numero, agencia, banco, saldo);
		this.chequeEspecial = saldo * 4;
	}

	public double getSaldo() {
		return this.chequeEspecial + this.saldo;
	}

	@Override
	public double getSacar() {
		// TODO Auto-generated method stub
		return this.saldo = saldo - sacar;
	}

}
