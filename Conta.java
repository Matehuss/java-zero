package Conta;

public abstract class Conta {
	
	private String nome;
	private int numero;
	private int agencia;
	private String banco;
	protected double saldo;
	protected double sacar;
	protected double depositar;
	
	public abstract double getSacar();

	public void setSacar(double sacar) {
		System.out.println("Voc� sacou R$"+sacar );
		this.sacar = sacar;
	}

	public double getDepositar() {
		return 	this.saldo =  saldo + depositar;
	}

	public void setDepositar(double depositar) {
		this.depositar = depositar;
		
	}

	public Conta(String nome, int numero, int agencia, String banco, double saldo) {
		super();
		this.numero = numero;
		this.agencia = agencia;
		this.banco = banco;
		this.saldo = saldo;
	}
	
	

	@Override
	public String toString() {
		return "Conta [nome=" + nome + ", numero=" + numero + ", agencia=" + agencia + ", banco=" + banco + ", saldo="
				+ saldo + ", sacar=" + sacar + ", depositar=" + depositar + "]";
	}

	public int getNumero() {
		return numero;
	}



	public void setNumero(int numero) {
		this.numero = numero;
	}


	public int getAgencia() {
		return agencia;
	}


	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}


	public String getBanco() {
		return banco;
	}


	public void setBanco(String banco) {
		this.banco = banco;
	}


	public abstract double getSaldo();


	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}


}
