package Conta;

public class ContaSalario extends Conta{

	private  int quantidade = 0;

	public ContaSalario(String nome, int numero, int agencia, String banco, double saldo) {
		super(nome, numero, agencia, banco, saldo);
	}

	public double getSaldo() {
		return saldo;
	}

	public double getSacar() {
		if(quantidade < 2){
			saldo = saldo - sacar;
			
			quantidade++;
		} else {
		System.out.println("Voc� atingiu o limite de saque num mesmo dia, somente amanh� poder� sacar novamente.");
		return this.saldo;
		}
		return saldo;
	}

}