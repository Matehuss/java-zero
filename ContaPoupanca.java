package Conta;

public class ContaPoupanca extends Conta{
	
	private int diaAniversarioConta;
	private double taxaDeJuros;
	
	
	
	public ContaPoupanca(String nome, int numero, int agencia, String banco, double saldo, int diaAniversarioConta,
			double taxaDeJuros) {
		super(nome, numero, agencia, banco, saldo);
		this.diaAniversarioConta = diaAniversarioConta;
		this.taxaDeJuros = taxaDeJuros;
	}

	
	public double getSaldo() {
		return this.saldo + (this.saldo * this.taxaDeJuros);
	}


	@Override
	public double getSacar() {
		// TODO Auto-generated method stub
		return this.saldo = saldo - sacar;
	}



}
 